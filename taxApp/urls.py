"""taxPro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from taxApp import views

urlpatterns = [
    path('', views.base,name="home"),
    path('get_your_ca',views.get_your_ca,name='get_your_ca'),
    path('form16',views.form16,name='form16'),
    path('refund_status',views.refund_status,name='refund_status'),
    path('file_your_return',views.file_your_return,name='file_your_return'),
    path('plan_tax',views.plan_tax,name='plan_tax'),
    path('nri_taxtation',views.nri_taxtation,name='nri_taxtation'),
    path('form12BB', views.form12BB, name='form12BB'),
    path('hra_calculator', views.hra_calculator, name='hra_calculator'),
    path('receipt_generate', views.receipt_generate, name='receipt_generate'),
    path('income_tax_calculator', views.income_tax_calculator, name='income_tax_calculator'),
    path('gst_registration', views.gst_registration, name='gst_registration'),
    path('income_tax_calculator', views.income_tax_calculator, name='income_tax_calculator'),
    path('gst_registration', views.gst_registration, name='gst_registration'),
    path('msme', views.msme, name='msme'),
    path('glossary', views.glossary, name='glossary'),
    path('gst_return_filing', views.gst_return_filing, name='gst_return_filing'),
    path('newold_tax_regime', views.newold_tax_regime, name='newold_tax_regime'),
    path('faqs', views.faqs, name='faqs'),
    path('financial_details', views.financial_details, name='financial_details'),
    path('get_ca_form', views.get_ca_form, name='get_ca_form'),
]

