from django.shortcuts import render,redirect
from .models import form16_model
import PyPDF2
# Create your views here.

def file_your_return(request):
    if request.method=='POST':
        salary=int(request.POST.get('salaryvalue'))
        business=int(request.POST.get('businessvalue'))
        houseproperty=int(request.POST.get('housepropertyvalue'))
        capitalgains=int(request.POST.get('capitalgainsvalue'))
        foreignincome=int(request.POST.get('foreignincomevalue'))
        otherincome=int(request.POST.get('otherincomevalue'))

        print(salary,business,houseproperty,capitalgains,foreignincome,otherincome)

        if salary == 1 and (business==0 or business==1 or otherincome==0 or otherincome==1 or houseproperty==0 or houseproperty ==1) and capitalgains==0 and foreignincome==0:
            return redirect('form16')
        elif (capitalgains==1 or foreignincome==1) and (salary == 1 or salary==0)and (business==0 or business==1 or otherincome==0 or otherincome==1 or houseproperty==0 or houseproperty ==1) :
            return redirect('get_your_ca')
        elif salary==0 and capitalgains==0 and foreignincome ==0 and (business==0 or business==1 or otherincome==0 or otherincome==1 or houseproperty==0 or houseproperty ==1):
            return redirect('financial_details')
    return render(request,'taxtation/file_your_return.html')

def financial_details(request):
    return render(request,'financial_details.html')

def base(request):
    return render(request,'base.html')

def glossary(request):
    return render(request,'taxtation/glossary.html')

def gst_return_filing(request):
    return render(request,'taxtation/gst_return_filing.html')

def newold_tax_regime(request):
    return render(request,'taxtation/newold_tax_regime.html')

def faqs(request):
    return render(request,'taxtation/faqs.html')

def gst_registration(request):
    return render(request,'taxtation/gst_registration.html')

def msme(request):
    return render(request,'taxtation/msme.html')

def income_tax_calculator(request):
    return render(request,'taxtation/income_tax_calculator.html')



def get_your_ca(request):
    if request.method=='POST':
        return render(request,'taxtation/get_ca_form.html')
    return render(request,'taxtation/get_your_ca.html')

def get_ca_form(request):
    return render(request,'taxtation/get_ca_form.html')


def plan_tax(request):
    return render(request,'taxtation/plan_tax.html')


def form12BB(request):
    return render(request,'taxtation/form12BB.html')

def receipt_generate(request):
    return render(request,'taxtation/receipt_generate.html')

def hra_calculator(request):
    return render(request,'taxtation/hra_calculator.html')

def nri_taxtation(request):
    return render(request,'taxtation/nri_taxtation.html')


import os
def form16(request):
    print(request.method)
    if request.method=='POST':
        file16=request.FILES['fileupload16']


        f=form16_model(file=file16)
        f.save()
        f1 = open('C:\\Users\\Admin\\PycharmProjects\\taxPro\\static\\files\\'+file16.name, 'rb')
        f1.seek(0, os.SEEK_END)  # seek to end of file; f.seek(0, 2) is legal
        pdfReader = PyPDF2.PdfFileReader(f1)
        information = pdfReader.getDocumentInfo()
        print(pdfReader.numPages)
        pageObj = pdfReader.getPage(0)
        print('aaa',pageObj.extractText(),information)

        first_name=request.POST.get('first_name')
        last_name=request.POST.get('last_name')
        email=request.POST.get('email')
        mobile_number=request.POST.get('mobile_number')

    return render(request,'taxtation/form16.html')



def refund_status(request):
    return render(request,'taxtation/refund_status.html')

