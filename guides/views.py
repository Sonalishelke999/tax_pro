from django.shortcuts import render


def aadhar(request):
    return render (request,'guides/aadhar.html')

def pan(request):
    return render(request,'guides/pan.html')

def capitalgain(request):
    return render(request,'guides/capitalgain.html')

def eitr(request):
    return render(request,'guides/eitr.html')

def houseproperty(request):
    return render (request,'guides/houseproperty.html')

def salaryincome(request):
    return render (request,'guides/salaryincome.html')

def section80deduction(request):
    return render (request,'guides/section80deduction.html')

def gst(request):
    return render (request,'guides/gst.html')

def gstsystem(request):
    return render (request,'guides/gstsystem.html')

def gstregistration(request):
    return render (request,'guides/gstregistration.html')

def inputtaxcredit(request):
    return render (request,'guides/inputtaxcredit.html')

def gstprocedure(request):
    return render (request,'guides/gstprocedure.html')

def gstreturn(request):
    return render (request,'guides/gstreturn.html')

def gstewaybill(request):
    return render (request,'guides/gstewaybill.html')

def contact(request):
    return render (request,'guides/contact.html')





# Create your views here.
