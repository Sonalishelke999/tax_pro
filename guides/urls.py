from django.conf.urls.static import static
from taxPro import settings
from django.urls import path
from . import views

urlpatterns = [
path('aadhar', views.aadhar, name='aadhar'),
path('pan', views.pan, name='pan'),
path('capitalgain', views.capitalgain, name='capitalgain'),
path('eitr', views.eitr, name='eitr'),
path('houseproperty', views.houseproperty, name='houseproperty'),
path('salaryincome', views.salaryincome, name='salaryincome'),
path('section80deduction', views.section80deduction, name='section80deduction'),
path('gst', views.gst, name='gst'),
path('gstsystem', views.gstsystem, name='gstsystem'),
path('gstregistration', views.gstregistration, name='gstregistration'),
path('inputtaxcredit', views.inputtaxcredit, name='inputtaxcredit'),
path('gstprocedure', views.gstprocedure, name='gstprocedure'),
path('gstreturn', views.gstreturn, name='gstreturn'),
path('gstewaybill', views.gstewaybill, name='gstewaybill'),
path('gstewaybill', views.gstewaybill, name='gstewaybill'),
path('contact', views.contact, name='contact'),
]