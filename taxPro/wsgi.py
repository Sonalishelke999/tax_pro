"""
WSGI config for taxPro project.

It exposes the WSGI callable as a.txt module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'taxPro.settings')

application = get_wsgi_application()
